document.addEventListener("DOMContentLoaded", function () {
    var Tour = /** @class */ (function () {
        function Tour(element, tourPrice, countryPrice, hotelPrice, levelPrice, typeTransp, numDays, numPers, numChildren) {
            this.el = element;
            this.tourPrice = tourPrice;
            this.countryPrice = countryPrice;
            this.hotelPrice = hotelPrice;
            this.levelPrice = levelPrice;
            this.typeTransp = typeTransp;
            this.numDays = numDays;
            this.numPers = numPers;
            this.numChildren = numChildren;
            var boxPrice = this.el.querySelector("#boxPrice");
            var boxPers = this.el.querySelector("#boxPers");
            var boxPhoto = this.el.querySelector("#photo");
            boxPers.innerHTML = "";
            boxPrice.innerHTML = "";
            boxPhoto.innerHTML = "";
            var price, totalPrice, html;
            if (this.numDays >= 10) {
                price =
                    this.tourPrice *
                        this.countryPrice *
                        this.hotelPrice *
                        this.levelPrice *
                        0.1;
                html = "<p>\u0423 \u0432\u0430\u0441 10% \u0441\u043A\u0438\u0434\u043A\u0430, \u0442\u0430\u043A \u043A\u0430\u043A \u0434\u043B\u0438\u0442\u0435\u043B\u044C\u043D\u043E\u0441\u0442\u044C \u0442\u0443\u0440\u0430 \u0431\u043E\u043B\u044C\u0448\u0435 10 \u0434\u043D\u0435\u0439.</p>\n                        <br>\n                        <p> \u0412\u044B \u0432\u044B\u0431\u0440\u0430\u043B\u0438 " + 4 + "-\u0437\u0432\u0435\u0437\u0434\u043E\u0447\u043D\u044B\u0439 \u043E\u0442\u0435\u043B\u044C, \n                        \u0446\u0435\u043D\u0430 \u0437\u0430 1 \u0434\u0435\u043D\u044C \u0441\u043E\u0441\u0442\u0430\u0432\u043B\u044F\u0435\u0442 : " + price + "</p>\n                        <p>\n \u0426\u0435\u043D\u0430 \u043D\u0430 " + this.numDays + " \u0434\u043D\u0435\u0439: " + price *
                    this.numDays + "</p>\n\n                        <p></p>";
                totalPrice = price * this.numDays - numChildren * 10;
            }
            else {
                price =
                    this.tourPrice *
                        this.countryPrice *
                        this.hotelPrice *
                        this.levelPrice;
                html = "<p>\u0423 \u0432\u0430\u0441 \u043D\u0435\u0442 \u0441\u043A\u0438\u0434\u043A\u0438, \u0442\u0430\u043A \u043A\u0430\u043A \u0434\u043B\u0438\u0442\u0435\u043B\u044C\u043D\u043E\u0441\u0442\u044C \u0442\u0443\u0440\u0430 \u043C\u0435\u043D\u044C\u0448\u0435 10 \u0434\u043D\u0435\u0439.</p>\n                        <br>\n                        <p> \u0412\u044B \u0432\u044B\u0431\u0440\u0430\u043B\u0438 " + 4 + "-\u0437\u0432\u0435\u0437\u0434\u043E\u0447\u043D\u044B\u0439 \u043E\u0442\u0435\u043B\u044C, \n                        \u0446\u0435\u043D\u0430 \u0437\u0430 1 \u0434\u0435\u043D\u044C \u0441\u043E\u0441\u0442\u0430\u0432\u043B\u044F\u0435\u0442: " + price + "</p>\n                        <p>\n \u0426\u0435\u043D\u0430 \u043D\u0430 " + this.numDays + " \u0434\u043D\u0435\u0439: " + price *
                    this.numDays + "</p>\n                        \n                        <p></p>";
                totalPrice = price * this.numDays - numChildren * 10;
            }
            var image = "images/5.jpg";
            var htmlT = "<p>\u041E\u043A\u043E\u043D\u0447\u0430\u0442\u0435\u043B\u044C\u043D\u0430\u044F \u0446\u0435\u043D\u0430 \u0437\u0430 \u0442\u0443\u0440: " + totalPrice + "</p>";
            var htmlP = "<img src=\"" + image + "\">";
            boxPers.insertAdjacentHTML("afterbegin", html);
            boxPrice.insertAdjacentHTML("afterbegin", htmlT);
            boxPhoto.insertAdjacentHTML("afterbegin", htmlP);
        }
        Tour.prototype.open = function () {
            this.el.style.display = "block";
        };
        return Tour;
    }());
    constructorSelect("http://localhost:3000/tours", "#inputState");
    constructorSelect("http://localhost:3000/countrys", "#inputCountry");
    constructorSelect("http://localhost:3000/hotels", "#inputHotel");
    document.querySelector("#but").addEventListener("click", function () {
        var element = document.querySelector("#rezultat");
        var tourPrice = getValue("#inputState");
        var countryPrice = getValue("#inputCountry");
        var hotelPrice = getValue("#inputHotel");
        var levelPrice = getValue("#inputLevel");
        var typeTransp = getValue("#inputTr");
        var numDays = parseFloat(document.querySelector("#numNigths").value);
        var numPers = parseFloat(document.querySelector("#numPers").value);
        var numChildren = parseFloat(document.querySelector("#numChild").value);
        var tourPerson = new Tour(element, tourPrice, countryPrice, hotelPrice, levelPrice, typeTransp, numDays, numPers, numChildren);
        tourPerson.open();
    });
    //Все функции
    //====Функция fetch которая  строит в select
    function constructorSelect(url, id) {
        fetch(url)
            .then(function (response) {
            return response.json();
        })
            .then(function (data) {
            var input = document.querySelector(id);
            var dataJSON = data.map(function (item) {
                return "<option value=\"" + item.price + "\">" + item.name + "</option>";
            });
            input.insertAdjacentHTML("beforeend", dataJSON.join(" "));
        });
    }
    //===== Функция которая берет выбранный элемент в select
    function getValue(id) {
        var select = document.querySelector(id);
        var price = select.options[select.selectedIndex].value;
        return price;
    }
});
