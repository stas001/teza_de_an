document.addEventListener("DOMContentLoaded", () => {
  class Tour {
    el: HTMLElement;
    tourPrice: number;
    countryPrice: number;
    hotelPrice: number;
    levelPrice: number;
    typeTransp: number;
    numDays: number;
    numPers: number;
    numChildren: number;
    constructor(
      element: HTMLElement,
      tourPrice: number,
      countryPrice: number,
      hotelPrice: number,
      levelPrice: number,
      typeTransp: number,
      numDays: number,
      numPers: number,
      numChildren: number
    ) {
      this.el = element;

      this.tourPrice = tourPrice;
      this.countryPrice = countryPrice;
      this.hotelPrice = hotelPrice;
      this.levelPrice = levelPrice;
      this.typeTransp = typeTransp;
      this.numDays = numDays;
      this.numPers = numPers;
      this.numChildren = numChildren;

      let boxPrice: HTMLElement = this.el.querySelector("#boxPrice");
      let boxPers: HTMLElement = this.el.querySelector("#boxPers");
      let boxPhoto: HTMLElement = this.el.querySelector("#photo");
      boxPers.innerHTML = "";
      boxPrice.innerHTML = "";
      boxPhoto.innerHTML = "";
      let price, totalPrice, html;
      if (this.numDays >= 10) {
        price =
          this.tourPrice *
          this.countryPrice *
          this.hotelPrice *
          this.levelPrice *
          0.1;
        html = `<p>У вас 10% скидка, так как длительность тура больше 10 дней.</p>
                        <br>
                        <p> Вы выбрали ${4}-звездочный отель, 
                        цена за 1 день составляет : ${price}</p>
                        <p>\n Цена на ${this.numDays} дней: ${price *
          this.numDays}</p>

                        <p></p>`;
        totalPrice = price * this.numDays - numChildren * 10;
      } else {
        price =
          this.tourPrice *
          this.countryPrice *
          this.hotelPrice *
          this.levelPrice;
        html = `<p>У вас нет скидки, так как длительность тура меньше 10 дней.</p>
                        <br>
                        <p> Вы выбрали ${4}-звездочный отель, 
                        цена за 1 день составляет: ${price}</p>
                        <p>\n Цена на ${this.numDays} дней: ${price *
          this.numDays}</p>
                        
                        <p></p>`;

        totalPrice = price * this.numDays - numChildren * 10;
      }
      const image = "images/5.jpg";
      const htmlT = `<p>Окончательная цена за тур: ${totalPrice}</p>`;
      const htmlP = `<img src="${image}">`;
      boxPers.insertAdjacentHTML("afterbegin", html);
      boxPrice.insertAdjacentHTML("afterbegin", htmlT);
      boxPhoto.insertAdjacentHTML("afterbegin", htmlP);
    }

    open() {
      this.el.style.display = "block";
    }
  }

  constructorSelect("http://localhost:3000/tours", "#inputState");
  constructorSelect("http://localhost:3000/countrys", "#inputCountry");
  constructorSelect("http://localhost:3000/hotels", "#inputHotel");

  document.querySelector("#but").addEventListener("click", () => {
    let element: HTMLElement = document.querySelector("#rezultat");
    const tourPrice = getValue("#inputState");
    const countryPrice = getValue("#inputCountry");
    const hotelPrice = getValue("#inputHotel");
    const levelPrice = getValue("#inputLevel");
    const typeTransp = getValue("#inputTr");
    const numDays = parseFloat(
      (<HTMLInputElement>document.querySelector("#numNigths")).value
    );
    const numPers = parseFloat(
      (<HTMLInputElement>document.querySelector("#numPers")).value
    );
    const numChildren = parseFloat(
      (<HTMLInputElement>document.querySelector("#numChild")).value
    );

    let tourPerson = new Tour(
      element,
      tourPrice,
      countryPrice,
      hotelPrice,
      levelPrice,
      typeTransp,
      numDays,
      numPers,
      numChildren
    );
    tourPerson.open();
  });

  //Все функции
  //====Функция fetch которая  строит в select
  function constructorSelect(url, id) {
    fetch(url)
      .then(response => {
        return response.json();
      })
      .then(data => {
        var input = document.querySelector(id);
        var dataJSON = data.map(function(item) {
          return `<option value="${item.price}">${item.name}</option>`;
        });

        input.insertAdjacentHTML("beforeend", dataJSON.join(" "));
      });
  }

  //===== Функция которая берет выбранный элемент в select
  function getValue(id) {
    let select = document.querySelector(id);
    let price = select.options[select.selectedIndex].value;
    return price;
  }
});
