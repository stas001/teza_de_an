document.addEventListener('DOMContentLoaded', function () {
    var url = 'http://localhost:3000/students';
    document.querySelector('#download').addEventListener('click', function () {
        document.querySelector('#table').innerHTML = "";
        fetch(url)
            .then(function (response) {
            return response.json();
        })
            .then(function (data) {
            var table = document.querySelector('#table');
            var html = data.map(function (item) {
                return "<tr><th scope=\"row\">" + item.id + "</th><th>" + item.name + "</th><th>" + item.surname + "</th><th>" + item.group + "</th>\n                        <th>" + item.marks + "</th></tr>";
            });
            table.insertAdjacentHTML('beforeend', html.join(' '));
        });
    });
});
