document.addEventListener('DOMContentLoaded', function () {
    var url = 'http://localhost:3000/cards';
    fetch(url)
        .then(function (response) {
        return response.json();
    })
        .then(function (data) {
        var cards = document.querySelector('#cards');
        var html = data.map(function (item) {
            return "<div class=\"card\" id=\"" + item.id + "\">\n                            <img class=\"card-img-top\" src=\"" + item.image + "\" alt=\"...\">\n                            <div class=\"card-body\">\n                                <p>\u0421\u0442\u0440\u0430\u043D\u0430: " + item.titleCountry + ", \u0433\u043E\u0440\u043E\u0434: " + item.titleCity + "</p>\n                                <p>\u041D\u0430\u0437\u0432\u0430\u043D\u0438\u0435 \u0442\u0443\u0440\u0430: " + item.titleTour + "</p>\n                                <p>\u041C\u0430\u0440\u0448\u0440\u0443\u0442: " + item.way + "</p>\n                                <p>\u041A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u043E \u0434\u043D\u0435\u0439: " + item.numD + "</p>\n                                <p>\u041A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u043E \u0437\u0432\u0435\u0437\u0434: " + item.numS + "</p>\n                                <p>\u0426\u0435\u043D\u0430: " + item.priceD + " \u0437\u0430 \u0434\u0435\u043D\u044C</p>\n                                <button type=\"button\" class=\"btn btn-warning\">\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C</button>\n                        </div></div>";
        });
        cards.insertAdjacentHTML('beforeend', html.join(''));
    });
    document.querySelector('#a1').addEventListener('click', function (event) {
        if (event.target) {
        }
    });
});
