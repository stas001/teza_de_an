
document.addEventListener('DOMContentLoaded', () =>{
    const url:string = 'http://localhost:3000/students'

    document.querySelector('#download').addEventListener('click', ()=>{
        document.querySelector('#table').innerHTML = ""
        fetch(url)
        .then((response)=>{
            return response.json()
        })
        .then((data)=>{
            
            let table = document.querySelector('#table')
            let html:any = data.map((item)=>{
                return `<tr><th scope="row">${item.id}</th><th>${item.name}</th><th>${item.surname}</th><th>${item.group}</th>
                        <th>${item.marks}</th></tr>`
            })
            table.insertAdjacentHTML('beforeend', html.join(' '))
        })
    })

})